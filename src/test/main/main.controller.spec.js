(function() {
	'use strict';

	describe('controllers', function(){

		beforeEach(module('eWallet'));

		var $location, $route, $rootScope;

		beforeEach(inject(function(_$location_, _$route_, _$rootScope_){
			$location = _$location_;
			$route = _$route_;
			$rootScope = _$rootScope_;
		}));

		it('should load the index page on successful load of /', function(){
			expect($location.path()).toBe('');

			$location.path('/');

        // The router works with the digest lifecycle, wherein after the location is set,
        // it takes a single digest loop cycle to process the route,
        // transform the page content, and finish the routing.
        // In order to trigger the location request, we’ll run a digest cycle (on the $rootScope) 
        // and check that the controller is as expected.
        $rootScope.$digest();

        expect($location.path()).toBe( '/' );
        expect($route.current.controller).toBe('MainController');
    	});

		it('should redirect to the index path on non-existent route', function(){
			expect($location.path()).toBe('');

			$location.path('/a/non-existent/route');

			$rootScope.$digest();

			expect($location.path()).toBe( '/' );
			expect($route.current.controller).toBe('MainController');
		});
	});
})();