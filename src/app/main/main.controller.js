(function() {
	'use strict';

	angular
	.module('eWallet')
	.controller('MainController', MainController);

	/** @ngInject */
	function MainController($auth) {
		var vm = this;
		vm.isAuthenticated = function() {
			return $auth.isAuthenticated();
		};
	}
})();