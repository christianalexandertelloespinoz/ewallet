(function() {
  'use strict';

  angular
    .module('eWallet', ['ngRoute', 'ui.bootstrap', 'indexedDB', 'ngTable', 
    	'frapontillo.bootstrap-switch', 'pascalprecht.translate', 'tmh.dynamicLocale', 'ngCookies', 
    	'satellizer', 'toastr', 'angular-intro', 'ngMessages', 'slideMenu']);
})();