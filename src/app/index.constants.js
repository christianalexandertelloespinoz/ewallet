(function() {
	'use strict';

	angular
	.module('eWallet')
	.constant('LOCALES', {
		'locales': {
			'es_PE': 'Español',
			'en_US': 'English'
		},
		'preferredLocale': 'es_PE'
	});
})();