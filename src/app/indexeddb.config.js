(function() {
	'use strict';

	angular
	.module('eWallet')
	.config(indexeddbConfig);

	function indexeddbConfig($indexedDBProvider) {
		$indexedDBProvider
		.connection('eWallet')
		.upgradeDatabase(2, function(event, db, tx){      	
			var objStore = db.createObjectStore('moneda', {keyPath: 'ssn', autoIncrement:true});
			objStore.createIndex('codigo_idx', 'codigo', {unique: true});
			objStore.createIndex('pais_idx', 'pais', {unique: false});
			objStore.createIndex('simbolo_idx', 'simbolo', {unique: false});
		})
		.upgradeDatabase(3, function(event, db, tx){        
			var objStore = db.createObjectStore('registro', {keyPath: 'ssn', autoIncrement:true});
			objStore.createIndex('moneda_idx', 'moneda_id', {unique: false});
			objStore.createIndex('monto_idx', 'monto', {unique: false});        
		})
		.upgradeDatabase(4, function(event, db, tx){        
			var objStore = db.createObjectStore('cuenta', {keyPath: 'ssn', autoIncrement:true});			  
		});
	}

})();