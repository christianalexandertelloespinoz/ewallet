(function() {
	'use strict';	
	angular
	.module('eWallet')
	.controller('LanguageSelectController', LanguageSelectController);

	/** @ngInject */
	function LanguageSelectController(localeService) {
		var vm = this;
		vm.currentLocaleDisplayName = localeService.getLocaleDisplayName();
		vm.localesDisplayNames = localeService.getLocalesDisplayNames();
		vm.visible = vm.localesDisplayNames &&
		vm.localesDisplayNames.length > 1;
		
		vm.changeLanguage = function (locale) {
			localeService.setLocaleByDisplayName(locale);
			vm.currentLocaleDisplayName = locale;
		};
	}
})();