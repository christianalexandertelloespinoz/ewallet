(function() {
	'use strict';
	angular
	.module('eWallet')
	.directive('languageSelect', languageSelect);
	/** @ngInject */
	function languageSelect() {
		var directive = {
			restrict: 'E',
			templateUrl: 'app/componentes/languageSelect/languageSelect.html',
			controller: 'LanguageSelectController',
			controllerAs: 'ls',
			bindToController: true
		};

		return directive;
	}
})();