(function() {
	'use strict';

	angular
	.module('eWallet')
	.config(function ($translateProvider, LOCALES) {
		$translateProvider.useMissingTranslationHandlerLog();
		$translateProvider.useStaticFilesLoader({
        	prefix: 'app/resources/locale-',// path to translations files
        	suffix: '.json'// suffix, currently- extension of the translations
        });    	
    	$translateProvider.preferredLanguage(LOCALES.preferredLocale);// is applied on first load
    	$translateProvider.useLocalStorage();// saves selected language to localStorage
    	$translateProvider.useSanitizeValueStrategy(null);
    })
	.config(function (tmhDynamicLocaleProvider) {
		tmhDynamicLocaleProvider.localeLocationPattern('bower_components/angular-i18n/angular-locale_{{locale}}.js');
	})
	.config(function ($authProvider) {
		$authProvider.loginUrl = "/webresources/auth/login";
		$authProvider.signupUrl = "/webresources/auth/signup";
		$authProvider.tokenName = "token";
		$authProvider.tokenPrefix = "eWallet";
		$authProvider.google({
			url: '/webresources/auth/google',
			redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
			clientId: '869791590776-hcsdpdq2q9c54f2olku4ne8gesn6r0k8.apps.googleusercontent.com'
		});
		$authProvider.facebook({
			url: '/webresources/auth/facebook',			
			clientId: '512154298948615',
			requiredUrlParams: ['display', 'scope'],
			scope: ['email', 'user_friends'],
			scopeDelimiter: ',',
			display: 'popup',
			type: '2.0',
			popupOptions: { width: 580, height: 400 }
		});
	});
})();