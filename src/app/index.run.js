(function() {
	'use strict';

	angular
	.module('eWallet')
	.run(runBlock);

	/** @ngInject */
	function runBlock($log, $translate, tmhDynamicLocale) {
		var currentLocale = $translate.proposedLanguage() || $translate.use();
		tmhDynamicLocale.set(currentLocale.toLowerCase().replace(/_/g, '-'));
		$log.debug('runBlock end');
	}

})();
