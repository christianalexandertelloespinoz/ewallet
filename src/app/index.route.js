(function() {
  'use strict';

  angular
  .module('eWallet')
  .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
    .when('/', {
      templateUrl: 'app/registro/nuevo.html',
      controller: 'RegistroController',
      controllerAs: 'main',
      resolve: {
        loginRequired: loginRequired
      }
    })
    .when('/config', {
      templateUrl: 'app/maestros/config.html',
      controller: 'ConfigController',
      controllerAs: 'conf',
      resolve: {
        loginRequired: loginRequired
      }
    })
    .when('/signup', {
      templateUrl: 'app/auth/signup.html',
      controller: 'AuthController',
      controllerAs: 'auth',
      resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
    })
    .when('/login', {
      templateUrl: 'app/auth/login.html',
      controller: 'AuthController',
      controllerAs: 'auth',
      resolve: {
          skipIfLoggedIn: skipIfLoggedIn
        }
    })
    .when('/logout', {
      template: null,
      controller: 'LogOutController',      
    })    
    .otherwise({
      redirectTo: '/'
    });

    function skipIfLoggedIn($q, $auth) {
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.reject();
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }
    
    function loginRequired($q, $location, $auth) {
      return;
      var deferred = $q.defer();
      if ($auth.isAuthenticated()) {
        deferred.resolve();
      } else {
        $location.path('/login');
      }
      return deferred.promise;
    }
  }

})();