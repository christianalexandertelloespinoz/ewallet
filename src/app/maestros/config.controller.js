(function() {
	'use strict';	
	angular
	.module('eWallet')
	.controller('ConfigController', ConfigController);

	/** @ngInject */
	function ConfigController(cuentaDB, $q, Cuenta) {
		var vm = this;		
		var editable;
		vm.submit = function() {
			console.log("Entro");
			cuentaDB.create(vm.cuenta).then(function(data){				
				if(!editable){
					vm.cuenta.ssn = data;
					vm.cuentas.unshift(vm.cuenta);	
				}
				initCuenta();
			}, function(err){
				console.log(err)
			});
		};
		vm.delete = function(id) {
			cuentaDB.remove(id).then(function(data){
				refreshList();
			}, function(err){
				console.log(err)
			});
		};
		vm.edit = function(cuenta) {
			vm.cuenta = cuenta;
			editable = true;
		};
		vm.cancel = function() {
			initCuenta();
		};
		function refreshList(){
			var deferred = $q.defer();			
			cuentaDB.listAll().then(function(data){
				vm.cuentas=data;
				deferred.resolve();					
			}, function(err){
				console.log(err);
			});			
			return deferred.promise;
		};
		function initCuenta(){			
			vm.cuenta = new Cuenta();			
			editable = false;
		}
		function init(){			
			refreshList().then(function(data){				
				initCuenta();
			}, function(err){
				console.log(err);
			});		
		}  
		init();
	}
})();