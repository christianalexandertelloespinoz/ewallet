(function() {
	'use strict';

	angular
	.module('eWallet')
	.factory('monedaDB', monedaDB);

	/** @ngInject */
	function monedaDB($indexedDB, $q) {
		var monedas = [];
		var getTodos = function(){
			var deferred = $q.defer();
			$indexedDB.openStore('moneda', function(store){
				store.getAll().then(function(_monedas) {
					monedas = _monedas;
					deferred.resolve(monedas);
				});
			});
			return deferred.promise;
		};
		return {			
			getTodos: getTodos
		};
	}
})();