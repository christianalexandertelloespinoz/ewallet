(function() {
	'use strict';

	angular
	.module('eWallet')
	.factory('cuentaDB', cuentaDB);

	/** @ngInject */
	function cuentaDB($indexedDB, $q) {
		var cuentas = [];
		var listAll = function(){
			var deferred = $q.defer();
			$indexedDB.openStore('cuenta', function(store){
				store.getAll().then(function(_cuentas) {
					cuentas = _cuentas;
					deferred.resolve(cuentas);
				});
			});
			return deferred.promise;
		};
		var create = function(cuenta){
			var deferred = $q.defer();
			$indexedDB.openStore('cuenta', function(store){
				store.upsert(cuenta).then(function(e){
					deferred.resolve(e[0]);
				});
			});
			return deferred.promise;
		};
		var remove = function(id){
			var deferred = $q.defer();
			$indexedDB.openStore('cuenta', function(store){
				store.delete(id).then(function(e){
					deferred.resolve("Cuenta eliminada");
				});
			});
			return deferred.promise;
		};
		return {			
			listAll: listAll,
			create: create,
			remove: remove
		};
	}
})();