(function() {
	'use strict';	
	angular
	.module('eWallet')
	.controller('AuthController', AuthController);

	/** @ngInject */
	function AuthController($auth, toastr, $location) {
		var vm = this;

		vm.signup = function() {
			$auth.signup(vm.user)
			.then(function() {
				$location.path('/login');
				toastr.info('You have successfully created a new account');
			})
			.catch(function(response) {
				toastr.error(response.data.message);
			});
		};
		vm.login = function(){
			$auth.login(vm.user)
			.then(function(){
            // Si se ha logueado correctamente, lo tratamos aquí.
            // Podemos también redirigirle a una ruta
            toastr.success('You have successfully signed in');
            $location.path('/');
        })
			.catch(function(response){
				toastr.error(response.data.message);
			});
		}
		vm.authenticate = function(provider) {
			$auth.authenticate(provider)
			.then(function() {
				toastr.success('You have successfully signed in with ' + provider);
				$location.path('/');
			})
			.catch(function(response) {
				console.log(response);				
			});
		};

		vm.IntroOptions = {
			steps:[
			{
				element: document.querySelector('#txtEmail'),
				intro: "This is the first tooltip."
			},
			{
				element: document.querySelectorAll('#txtPass')[0],
				intro: "<strong>You</strong> can also <em>include</em> HTML",
				position: 'right'
			},
			{
				element: '#btnSubmit',
				intro: 'More features, more fun.',
				position: 'left'
			},
			{
				element: '#btnSignUp',
				intro: "Another step.",
				position: 'bottom'
			},
			{
				element: '#btnFacebook',
				intro: 'Get it, use it.'
			}
			],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc:true,
			nextLabel: '<strong>NEXT!</strong>',
			prevLabel: '<span style="color:green">Previous</span>',
			skipLabel: 'Exit',
			doneLabel: 'Thanks'
		};

		vm.ShouldAutoStart = true;
	}
})();