(function() {
	'use strict';

	angular
	.module('eWallet')
	.controller('LogOutController', LogOutController);

	/** @ngInject */
	function LogOutController($location, $auth) {
		if (!$auth.isAuthenticated()) { return; }
		$auth.logout()
		.then(function() {
			console.log('You have been logged out');
			$location.path('/');
		});
	}
})();