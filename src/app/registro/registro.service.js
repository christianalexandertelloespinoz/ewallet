(function() {
	'use strict';
	angular
	.module('eWallet')
	.factory('registroDB', registroDB);
	/** @ngInject */
	function registroDB($indexedDB, $q) {		
		var getTodos = function(){
			var deferred = $q.defer();
			$indexedDB.openStore('registro', function(store){
				store.getAll().then(function(registros) {					
					deferred.resolve(registros);
				});
			});
			return deferred.promise;
		};
		var addTodo = function(registro){
			var deferred = $q.defer();
			$indexedDB.openStore('registro', function(store){
				store.upsert(registro).then(function(e){
					deferred.resolve(e[0]);
				});
			});
			return deferred.promise;
		};
		var deleteReg = function(id){
			var deferred = $q.defer();
			$indexedDB.openStore('registro', function(store){
				store.delete(id).then(function(e){
					deferred.resolve("Registro eliminado");
				});
			});
			return deferred.promise;
		};
		return {			
			addTodo: addTodo,
			getTodos: getTodos,
			deleteReg: deleteReg
		};
	}
})();