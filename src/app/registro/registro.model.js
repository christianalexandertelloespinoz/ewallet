(function() {
	'use strict';
	angular
	.module('eWallet')
	.factory('Registro', Registro);
	/** @ngInject */
	function Registro() {
		function Registro() {
			this.ssn;
			this.moneda = {};
			this.cuenta = {};
			this.monto;
			this.descripcion;
			this.isIngreso = false;
			this.fecha = new Date();			
		};
		Registro.prototype = {
			setMoneda: function (_moneda) {
				this.moneda.moneda_id = _moneda.ssn;
				this.moneda.simbolo = _moneda.simbolo;
			},
			setCuenta: function (_cuenta) {
				this.cuenta.cuenta_id = _cuenta.ssn;
				this.cuenta.nombre = _cuenta.nombre;
			}
		};
		return Registro;
	}
})();