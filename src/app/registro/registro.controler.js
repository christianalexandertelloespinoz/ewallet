(function() {
	'use strict';	
	angular
	.module('eWallet')
	.controller('RegistroController', RegistroController);

	/** @ngInject */
	function RegistroController(monedaDB, registroDB, $filter, cuentaDB, Registro, $q, $auth) {
		var vm = this;
		vm.monedas;
		vm.cuentas;
		vm.registro = new Registro();		
		vm.registros;
		vm.status = {
			opened: false
		};
		var editable;
		vm.open = function($event) {
			vm.status.opened = true;
		};		
		vm.dropboxitemselected = function (item) {			
			vm.registro.setMoneda(item);
		};
		vm.cuentaitemselected = function (item) {
			vm.registro.setCuenta(item);			
		};
		vm.submit = function() {						
			registroDB.addTodo(vm.registro).then(function(data){				
				if(!editable){
					vm.registro.ssn = data;
					vm.registros.unshift(vm.registro);	
				}				
				initRegistro();
			}, function(err){
				console.log(err)
			});
		};
		vm.delete = function(id) {
			registroDB.deleteReg(id).then(function(data){
				refreshRegistros();
			}, function(err){
				console.log(err)
			});
		};
		vm.edit = function(reg) {
			vm.registro = reg;
			editable = true;
		};
		vm.cancel = function() {
			initRegistro();
		};
		function refreshList(){
			var deferred = $q.defer();
			monedaDB.getTodos().then(function(data){
				vm.monedas=data;
				cuentaDB.listAll().then(function(data){
					vm.cuentas=data;
					deferred.resolve();					
				}, function(err){
					console.log(err);
				});
			}, function(err){
				console.log(err);
			});			
			refreshRegistros();
			return deferred.promise;
		};
		function refreshRegistros(){
			registroDB.getTodos().then(function(data){				
				vm.registros=$filter('orderBy')(data, 'snn', true);
			}, function(err){
				console.log(err);
			});
		}
		function initRegistro(){
			vm.registro = new Registro();				
			vm.registro.setMoneda(vm.monedas[0]);
			vm.registro.setCuenta(vm.cuentas[0]);
			editable = false;
		}
		function init(){
			refreshList().then(function(data){
				initRegistro();
			}, function(err){
				console.log(err);
			});		
		}  
		init();
	}
})();